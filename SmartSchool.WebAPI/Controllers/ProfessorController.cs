using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartSchool.WebAPI.Data;
using SmartSchool.WebAPI.Models;

namespace SmartSchool.WebAPI.Controllers
{
    [ApiController]
    [Route("api/professor")]
    public class ProfessorController : ControllerBase
    {
        private readonly SmartDataContext _context;
        public ProfessorController(SmartDataContext context)
        { 
            _context = context;
        }

        #region GETs

        //http://localhost:5000/api/professor 
        [HttpGet]       
        public IActionResult Get()
        {
            return Ok(_context.Professores);
        }

        //http://localhost:5000/api/professor/1
        [HttpGet ("{Id:int}")]       
        public IActionResult GetId(int Id)
        {
            var professorLocalizado = _context.Professores.AsNoTracking().FirstOrDefault(x => x.Id == Id);
            if (professorLocalizado == null) return BadRequest("Professor não localizado");

            return Ok(professorLocalizado);
        }

        //http://localhost:5000/api/professor/GetById?Id=1
        [HttpGet ("GetById")]
        public IActionResult GetById(int Id)
        {
            var professorLocalizado = _context.Professores.AsNoTracking().FirstOrDefault(x => x.Id == Id);
            if (professorLocalizado == null) return BadRequest("Professor não localizado");

            return Ok(professorLocalizado);
        }

        //http://localhost:5000/api/professor/GetByNome?Nome=Ailton
        [HttpGet ("GetByNome")]
        public IActionResult GetByNome(string Nome)
        {
            var professorLocalizado = _context.Professores.FirstOrDefault(x => x.Nome == Nome);
            if (professorLocalizado == null) return BadRequest("Professor não localizado");

            return Ok(professorLocalizado);
        }

        #endregion

        #region POST / PUT / PATCH / DELETE
        [HttpPost]
        public IActionResult Post(Professor professor)
        {
            _context.Add(professor);
            _context.SaveChanges();

            return Ok(professor.Nome + " - Professor salvo com sucesso ");
        }

        [HttpPatch ("{id}")]
        public IActionResult Patch(int id, Professor professor)
        {
            var professorLocalizado = _context.Professores.AsNoTracking().FirstOrDefault(x => x.Id == id);
            if (professorLocalizado == null) return BadRequest("Professor não foi localizado");

            _context.Update(professor);
            _context.SaveChanges();

            return Ok(professor.Nome + " - Professor atualizado com sucesso ");
        }

        [HttpPut ("{id}")]
        public IActionResult Put(int id, Professor professor)
        {
            var professorLocalizado = _context.Professores.AsNoTracking().FirstOrDefault(x => x.Id == id);
            if (professorLocalizado == null) BadRequest("Professor não foi localizado");

            _context.Update(professor);
            _context.SaveChanges();

            return Ok(professor.Nome + " - Professor atualizado com sucesso ");
        }

        [HttpDelete ("{id}")]
        public IActionResult Delete(int id)
        {
            var professorLocalizado = _context.Professores.AsNoTracking().FirstOrDefault(x => x.Id == id);
            if (professorLocalizado == null) BadRequest("Professor não foi localizado");

            _context.Remove(professorLocalizado);
            _context.SaveChanges();

            return Ok(" Professor deletado com sucesso ");
        }

        #endregion

    }
}