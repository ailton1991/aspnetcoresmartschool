using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartSchool.WebAPI.Data;
using SmartSchool.WebAPI.Models;

namespace SmartSchool.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AlunoController : ControllerBase
    {
        private readonly SmartDataContext _context;
        private readonly IRepository _repository;

        public AlunoController(IRepository repository , SmartDataContext context) 
        {
            _repository = repository;
            _context = context;    
        }

        // List<Aluno> ListaDeAlunos()
        // {
        //     return new List<Aluno>()
        //     {
        //          new Aluno() {
        //             Id = 1,
        //             Nome = "Ailton",
        //             Sobrenome = "Xavier da Silva Junior",
        //             Telefone = "21-979999817"
        //          },
        //          new Aluno() {
        //             Id = 2,
        //             Nome = "Hercules",
        //             Sobrenome = "Silva",
        //             Telefone = "21-2413-7343"
        //          },
        //          new Aluno() {
        //              Id = 3,
        //              Nome = "Camila",
        //              Sobrenome = "Oliveira",
        //              Telefone = "21-96999-9817"
        //          }
        //     };
        // }

        #region HTTPGET Vários Parâmetros

        [HttpGet("PegarValor")]
        public IActionResult PegarValor()
        {
            return Ok(_repository.pegaResposta());
        }

        //http://localhost:5000/api/aluno //-> Pega Todos
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.Alunos);
        }

        //http://localhost:5000/api/aluno/1    
        [HttpGet("{Id:int}")]
        public IActionResult GetById(int Id)
        {
             var aluno = _context.Alunos.FirstOrDefault(x => x.Id == Id);

             if(aluno == null)
                return BadRequest("Aluno não encontrado"); 

            return Ok(aluno);             
        }

        //http://localhost:5000/api/aluno/byid?Id=1 //-> QueryString
        [HttpGet("ById")]
        public IActionResult ByIdGet(int Id)
        {
            var aluno = _context.Alunos.FirstOrDefault(x => x.Id == Id);

            if(aluno == null)
                return BadRequest("Aluno não encontrado");

            return Ok(aluno);
        }

        //http://localhost:5000/api/aluno/Ailton
        [HttpGet("{Nome}")]
        public IActionResult GetByNome(string Nome)
        {
            var aluno = _context.Alunos.AsNoTracking().FirstOrDefault(x => x.Nome.Contains(Nome));

            if(aluno == null)
                return BadRequest("Aluno não encontrado em nossa Base de Dados");

            return Ok(aluno);
        }

        //http://localhost:5000/api/aluno/1/Ailton
        [HttpGet("{Id}/{Nome}")]
        public IActionResult GetByIdNome(int Id, string Nome) 
        {
            var aluno = _context.Alunos.AsNoTracking().FirstOrDefault(x => x.Id == Id);

            if(aluno != null)
                return BadRequest("Aluno não encontrado em nossa Base de Dados");

            return Ok(aluno);
        }

        //http://localhost:5000/api/aluno/byNome?nome=Ailton&sobrenome=Xavier
        [HttpGet("byNome")]
        public IActionResult GetByNomeSobrenome(string nome, string sobrenome)
        {
            var aluno = _context.Alunos.FirstOrDefault(x => x.Nome.Contains(nome) && x.Sobrenome.Contains(sobrenome));

            if(aluno == null)
                return BadRequest("Aluno não encontrado em nossa Base de Dados");

            return Ok(aluno);
        }

        #endregion




        #region POST / PUT / PATCH / DELETE

        //http:localhost:5000/api/aluno
        [HttpPost]
        public IActionResult postAluno(Aluno aluno)
        {
            var result = _repository.Add(aluno);
            //_context.Add(aluno);
            if (_repository.SaveChanges())
            {
                return Ok(aluno);
            }
            return BadRequest("Aluno Não Atualizado");
        }

        [HttpPut("{Id}")]
        public IActionResult putAluno(int Id, Aluno aluno)
        {
            var alunoEncontrado = _context.Alunos.AsNoTracking().FirstOrDefault(x => x.Id == Id);
            if (alunoEncontrado == null) return BadRequest("Aluno não encontrado");

            var result = _repository.Update(aluno);
            //_context.Add(aluno);
            if (_repository.SaveChanges())
            {
                return Ok(aluno);
            }
            return BadRequest("Aluno Não Cadastrado");
        }

        [HttpPatch("{Id}")]
        public IActionResult pacthAluno(int Id, Aluno aluno)
        {
            var alunoEncontrado = _context.Alunos.AsNoTracking().FirstOrDefault(x => x.Id == Id);
            if (alunoEncontrado == null) return BadRequest("Aluno não encontrado");

            var result = _repository.Update(aluno);
            //_context.Add(aluno);
            if (_repository.SaveChanges())
            {
                return Ok(aluno);
            }
            return BadRequest("Aluno Não Atualizado");
        }

        [HttpDelete("{Id}")]
        public IActionResult deleteAluno(int Id)
        {
            var alunoEncontrado = _context.Alunos.AsNoTracking().FirstOrDefault(x => x.Id == Id);
            if (alunoEncontrado == null) return BadRequest("Aluno não encontrado");

            var result = _repository.Delete(alunoEncontrado);
            //_context.Add(aluno);
            if (result && _repository.SaveChanges())
            {
                return Ok("Aluno Deletado");
            }
            return BadRequest("Aluno Não foi Deletado");
        }

        #endregion

    }
}