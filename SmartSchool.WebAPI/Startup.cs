using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SmartSchool.WebAPI.Data;
using Microsoft.EntityFrameworkCore;

namespace SmartSchool.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SmartDataContext>(
                context => context.UseSqlite(Configuration.GetConnectionString("Default"))
            );

            //services.AddSingleton<IRepository,Repository>(); //// Cria uma única instância do serviço quando é solicitado, pela primeira vez e reutiliza essa mesma instância em todos os locais em que esse serviço é necessário.
            //services.AddTransient<IRepository,Repository>(); //// Sempre gerará uma nova instância para cada item encontrado que possua tal dependências, ou seja se houver 5 dependências serão 5 instâncias diferentes: => Exemplo : Repository Aluno -> Vai preencher dados do Aluno porém não vai preencher dados AlunoDisciplina.
            services.AddScoped<IRepository,Repository>();    //// Garante que em uma requisição seja criada uma instância de uma classe, onde se houver outras dependências, seja utilizado essa única instância pra todos renovando somente nas requisições subsequentes mas, mantendo essa obrigatoriedade. => Exemplo : Repository Aluno quando houver uma requisição vai preencher a classe Aluno mas se houver uma outra requisição ALUNO_Disciplina vai ser preenchida na mesma instância.

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
