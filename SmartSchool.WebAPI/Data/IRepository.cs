namespace SmartSchool.WebAPI.Data
{
    public interface IRepository
    {
         string pegaResposta();
         bool Add<T>(T entity) where T : class;
         bool Update<T>(T entity) where T : class;
         bool Delete<T>(T entity) where T : class;
         bool SaveChanges();
    }
}