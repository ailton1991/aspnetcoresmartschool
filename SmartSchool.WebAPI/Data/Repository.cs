namespace SmartSchool.WebAPI.Data
{
    public class Repository : IRepository
    {
         private readonly SmartDataContext _context;
         public Repository(SmartDataContext context)
         {
            _context = context;
         }
        public bool Add<T>(T entity) where T : class
        {
            try
            {
                _context.Add(entity);
                //_context.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
            
        }
        public bool Update<T>(T entity) where T : class
        {
            try
            {
                _context.Update(entity);
                //_context.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        public bool Delete<T>(T entity) where T : class
        {
            try
            {
                _context.Remove(entity);
                //_context.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        public bool SaveChanges()
        {
            try
            {
                return (_context.SaveChanges() > 0);
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        public string pegaResposta()
        {
            return "Implementado =)";
        }
    }
}